/**
 * Created by zhoupei on 2019/4/17.
 */
$().ready(function () {

    $("#go").click(function () {
        var branch = $("#branch_name").val();
        var email = $("#email").val().trim();
        var debug = $("#debug").val();
        var sign = $("#sign").val();
        var message = $("#note").val();
        var big_data_env = $("#big_data_env").val();
        var it_api_env = $("#it_api_env").val();
        var package_channel = $("#package_channel").val();
        var intime_log_onoff = $("#intime_log_onoff").val();
        var local_log_onoff = $("#local_log_onoff").val();
        var console_log_onoff = $("#console_log_onoff").val();
        if (branch.indexOf("请") != -1) {
            $('.modal-body').html('打包分支必选哦！');
            $("#myModal").modal();
            return;
        }
        if (debug.indexOf("请") != -1) {
            $('.modal-body').html('打包方式必选哦！');
            $("#myModal").modal();
            return;
        }
        if (sign.indexOf("请") != -1) {
            $('.modal-body').html('签名方式必选哦！');
            $("#myModal").modal();
            return;
        }
        if (big_data_env.indexOf("请") != -1) {
            $('.modal-body').html('大数据环境必选哦！');
            $("#myModal").modal();
            return;
        }
        if (it_api_env.indexOf("请") != -1) {
            $('.modal-body').html('IT接口环境必选哦！');
            $("#myModal").modal();
            return;
        }
        if (package_channel.indexOf("请") != -1) {
            $('.modal-body').html('IOT渠道必选哦！');
            $("#myModal").modal();
            return;
        }
        if (intime_log_onoff.indexOf("请") != -1) {
            $('.modal-body').html('实时日志必选哦！');
            $("#myModal").modal();
            return;
        }
        if (local_log_onoff.indexOf("请") != -1) {
            $('.modal-body').html('本地日志是否开启必选哦！');
            $("#myModal").modal();
            return;
        }
        if (console_log_onoff.indexOf("请") != -1) {
            $('.modal-body').html('控制台输出必选哦！');
            $("#myModal").modal();
            return;
        }
         if (!email.length) {
            $('.modal-body').html('填个邮箱地址接受打包状态吧！');
            $("#myModal").modal();
            return;
        }
        console.log(Boolean(parseInt(console_log_onoff)));
        $.post('/preparePackage', {
            branch: branch,
            email: email,
            note: message,
            debug: debug,
            sign: sign,
            big_data_env: big_data_env,
            it_api_env: it_api_env,
            package_channel: package_channel,
            intime_log_onoff: Boolean(parseInt(intime_log_onoff)),
            local_log_onoff: Boolean(parseInt(local_log_onoff)),
            console_log_onoff: Boolean(parseInt(console_log_onoff))
        }, function (data) {
            console.log(data);
            if (data.code == 200) {
                var str = data.msg + email;
                $('.modal-body').html(str);
                $("#myModal").modal();

            }
            else {
                var str = data.msg;
                $('.modal-body').html(str);
                $("#myModal").modal("show");
            }
        });
    });
    $("#modal-dismis").click(function () {
        $("#myModal").modal("hide");
    });
});
function uploadPackage(packageid) {
    $.post('/upload_ipa', {packageid: packageid}, function (data) {
        console.log(data);
        if (data.code == 200) {
            var str = data.msg;
            $('.modal-body').html(str);
            $("#myModal").modal();
        } else {
            var str = data.msg;
            $('.modal-body').html(str);
            $("#myModal").modal();
        }
    })
}

function auto_complete_appstore() {
      var branch = $("#branch_name").val("DEV_D");
        var debug = $("#debug").val("2");
        var sign = $("#sign").val("4");
        var big_data_env = $("#big_data_env").val("release");
        var it_api_env = $("#it_api_env").val("release");
        var package_channel = $("#package_channel").val("c_appstore");
        var intime_log_onoff = $("#intime_log_onoff").val("0");
        var local_log_onoff = $("#local_log_onoff").val("0");
        var console_log_onoff = $("#console_log_onoff").val("0");
        $("#go").click();
}