/**
 * Created by zhoupei on 2019/4/17.
 */
$().ready(function () {

    $("#go").click(function () {
        var branch = $("#branch_name").val();
        var channel = $("#channel").val();
        var email = $("#email").val().trim();
        var message = $("#note").val();
        var pack_type = $("#pack_type").val();
        var debuggable = $("#debuggable").val();
        var it_api_env = $("#it_api_env").val();
        var iot_env = 'release';
        var intime_log_onoff = $("#intime_log_onoff").val();
        var console_log_onoff = $("#console_log_onoff").val();
        var jpush = $("#jpush").val();
        var app_signature = $("#appSignature").val();
        var proguard = $("#proguard").val();
        var firebase = $("#firebase").val();
        var xiaoneng = $("#xiaoneng").val();
        var big_data_env = $("#big_data_env").val();
        if (branch.indexOf("请") != -1) {
            $('.modal-body').html('打包分支必选哦！');
            $("#myModal").modal();
            return;
        }
        if (channel.indexOf("请") != -1) {
            $('.modal-body').html('渠道必选哦！');
            $("#myModal").modal();
            return;
        }
        if (pack_type.indexOf("请") != -1) {
            $('.modal-body').html('打包方式必选哦！');
            $("#myModal").modal();
            return;
        }
        if (big_data_env.indexOf("请") != -1) {
            $('.modal-body').html('大数据环境必选哦！');
            $("#myModal").modal();
            return;
        }
        if (it_api_env.indexOf("请") != -1) {
            $('.modal-body').html('IT接口环境必选哦！');
            $("#myModal").modal();
            return;
        }
        if (debuggable.indexOf("请") != -1) {
            $('.modal-body').html('是否可调式必选哦！');
            $("#myModal").modal();
            return;
        }
        if (intime_log_onoff.indexOf("请") != -1) {
            $('.modal-body').html('实时日志必选哦！');
            $("#myModal").modal();
            return;
        }
        if (jpush.indexOf("请") != -1) {
            $('.modal-body').html('极光推送方式必选哦！');
            $("#myModal").modal();
            return;
        }
        if (console_log_onoff.indexOf("请") != -1) {
            $('.modal-body').html('控制台输出必选哦！');
            $("#myModal").modal();
            return;
        }
        if (!email.length) {
            $('.modal-body').html('填个邮箱地址接受打包状态吧！');
            $("#myModal").modal();
            return;
        }
        if (app_signature.indexOf("请") != -1) {
            $('.modal-body').html('签名方式必选哦！');
            $("#myModal").modal();
            return;
        }
        if (firebase.indexOf("请") != -1) {
            $('.modal-body').html('谷歌推送方式必选哦！');
            $("#myModal").modal();
            return;
        }
        if (xiaoneng.indexOf("请") != -1) {
            $('.modal-body').html('小能必选哦！');
            $("#myModal").modal();
            return;
        }
        console.log(branch);
        if (branch.length) {
            $.post('/prepareAndroidPackage', {
                branch: branch,
                email: email,
                channel: channel,
                note: message,
                debuggable: Boolean(parseInt(debuggable)),
                it_api_env: it_api_env,
                iot_env: iot_env,
                intime_log_onoff: Boolean(parseInt(intime_log_onoff)),
                console_log_onoff: Boolean(parseInt(console_log_onoff)),
                jpush: jpush,
                app_signature: app_signature,
                proguard: proguard,
                firebase: firebase,
                xiaoneng: xiaoneng,
                big_data_env: big_data_env
            }, function (data) {
                console.log(data);
                if (data.code == 200) {
                    var str = data.msg + email;
                    $('.modal-body').html(str);
                    $("#myModal").modal();
                }
                else {
                    var str = data.msg;
                    $('.modal-body').html(str);
                    $("#myModal").modal("show");
                }
            });
        }
        else {
            $('.modal-body').html('打包分支，打包方式，打包环境必选哦！');
            $("#myModal").modal();
        }
    });
    $("#modal-dismis").click(function () {
        $("#myModal").modal("hide");
    });
});