#!/usr/bin/env bash
server_path=$(cd "$(dirname "$0")";pwd)

#/bin/sh {sh_path} {branch} {debug} {note} {email} {channel}
CUSTOM_BRANCH_NAME=${1}
CUSTOM_DEBUG=${2}
CUSTOM_MESSAGE=${3}
CUSTOM_EMAIL_ADDRESS=${4}
CUSTOM_CHANNEL=${5}
CUSTOM_FILE_NAME=${6}
CUSTOM_PACKAGE_ID=${7}
CUSTOM_PARAMETERS=${8}

echo $CUSTOM_PARAMETERS
cmd="./gradlew assemble${CUSTOM_CHANNEL}${CUSTOM_DEBUG}"

echo $cmd


API_INTERFACE="http://127.0.0.1:8811/v2/packageDidFinish"

gitPath="ssh://git@10.10.10.25:443/GlobalApp-Android"

timestamp="${CUSTOM_BRANCH_NAME}-`date +%s`"
cd ~/git_temp

mkdir ${timestamp}

cd ${timestamp}

git clone -b ${CUSTOM_BRANCH_NAME} ${gitPath}

cd "GlobalApp-Android"

`echo ${CUSTOM_PARAMETERS}>"./packaging.json"`

BUILD_GRADLE_FILES=`find . -name "build.gradle" | awk -F [\w] '{print $(NF)}'`
for ii in $BUILD_GRADLE_FILES
    do
    result=$(echo ${ii} | grep "GlobalRobotSDK/robot/")
     if [ "${result}" == "" ];then
        t=1
     else
        BUILD_GRADLE_FILE=${ii}
     fi
done
IOT_SDK_VERSION=`sed -n "s/.*lib_iot_client:\(.*\)'/\1/p" $BUILD_GRADLE_FILE`
echo $IOT_SDK_VERSION

`sh ${cmd}>>/tmp/package.log`

APK_PATH=(`find . -name "*.apk" | awk -F [\w] '{print $(NF)}'`)

CONFIG_JAVA_FILES=`find . -name "BuildConfig.java" | awk -F [\w] '{print $(NF)}'`
for i in $CONFIG_JAVA_FILES
do
     result=$(echo ${i} | grep "autoPackaging")
     if [ "${result}" == "" ];then
        t=1
     else
        CONFIG_JAVA_FILE=${i}
     fi
done


echo $CONFIG_JAVA_FILE
echo $BUILD_GRADLE_FILE
CUSTOM_VERSION_NUMBER=`sed -n 's/.*VERSION_NAME = "\(.*\)";/\1/p' $CONFIG_JAVA_FILE`
BUILD_TIME=`sed -n 's/.*buildNumber = "\(.*\)";/\1/p' $CONFIG_JAVA_FILE `


if [ "$APK_PATH" == "" ];then
    curl -d "email=$CUSTOM_EMAIL_ADDRESS&note=$CUSTOM_MESSAGE&msg='编译失败，请检查代码是否可有编译通过！'&packageid=$CUSTOM_PACKAGE_ID&version=''&iot_sdk_version=''&build_number=''&success=0&package_type=2" $API_INTERFACE
else
    echo $APK_PATH
     if [ "$CUSTOM_FILE_NAME" == "" ];then
        zip -q -r "$server_path/static/apks/$CUSTOM_PACKAGE_ID.zip"  './app/build/outputs'
    else
       zip -q -r "$server_path/static/apks/$CUSTOM_FILE_NAME.zip"  './app/build/outputs'
    fi
    curl -d "email=$CUSTOM_EMAIL_ADDRESS&note=$CUSTOM_MESSAGE&packageid=$CUSTOM_PACKAGE_ID&version=$CUSTOM_VERSION_NUMBER&iot_sdk_version=$IOT_SDK_VERSION&build_number=$BUILD_TIME&success=1&package_type=2" $API_INTERFACE
fi

#rm -rf ~/git_temp/$timestamp