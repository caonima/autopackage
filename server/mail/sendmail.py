#!/bin/env python
# coding: utf-8
import os, smtplib, mimetypes, ConfigParser, re
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from exchangelib import Account, Credentials, Message, Mailbox
from Classes.SingletonClass import MailSenderSingleton
def mysendmail(mailist, subject, msg, filename=None, format='plain'):
    USERNAME = '513402228@qq.com'
    try:
        message = MIMEMultipart()
        message.attach(MIMEText(msg))
        message = MIMEText(msg, format)
        message["Subject"] = subject
        message["From"] = USERNAME
        message["To"] = ";".join(mailist)
        message["Accept-Language"] = "zh-CN"
        message["Accept-Charset"] = "ISO-8859-1,utf-8"
        if filename != None and os.path.exists(filename):
            ctype, encoding = mimetypes.guess_type(filename)
            if ctype is None or encoding is not None:
                ctype = "application/octet-stream"
            maintype, subtype = ctype.split("/", 1)
            attachment = MIMEImage((lambda f: (f.read(), f.close()))(open(filename, "rb"))[0], _subtype=subtype)
            attachment.add_header("Content-Disposition", "attachment", filename=filename)
            message.attach(attachment)

        s = smtplib.SMTP_SSL("smtp.qq.com", 465)
        s.login(USERNAME, 'kcveghffvxeubhga')
        s.sendmail(USERNAME, mailist, message.as_string())
        s.quit()

        return True
    except Exception, errmsg:
        print "Send mail failed to : %s" % errmsg
        return False


def exchange_send_email(mailist, subject, msg):
    account = MailSenderSingleton().account()
    mails = [Mailbox(email_address=i) for i in mailist]
    m = Message(
            account=account,  # 之前申明的账户
            subject=subject,
            body=msg,
            to_recipients=mails
    )
    m.send()


if __name__ == "__main__":
    exchange_send_email(mailist=['pei.zhou@ecovacs.com'], subject=u'自动打包结果反馈',
                        msg='什么呢大声道'.decode('utf-8') + '\n\n' + '迪卡侬'.decode('utf-8'))
