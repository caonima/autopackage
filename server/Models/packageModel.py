# coding:utf-8
__author__ = 'zhoupei'
import bson
from mongoengine import *
from settings.setting import *
import time

eval(init_connection_string)

PACKAGE_TYPE_IOS = 1
PACKAGE_TYPE_ANDROID = 2

PACKAGE_PACKING = 1
PACKAGE_SUCCESS = 2
PACKAGE_FAILED = 3
PACKAGE_UPLOADING = 5
PACKAGE_UPLOADED = 4


class packageModel(Document):
    packageType = IntField()
    packageVersion = StringField()
    packageIOTVersion = StringField()
    packageBuildVersion = StringField()
    packageEmailList = ListField(default=[])
    packageState = IntField()
    packagePath = StringField()
    packageSymbolPath = StringField()
    packageTime = IntField()
    packageTimeString = IntField()
    packageConfig = StringField()
    packageNote = StringField()
