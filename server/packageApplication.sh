#!/usr/bin/env bash
server_path=$(cd "$(dirname "$0")";pwd)
export LC_ALL=zh_CN.GB2312;
export LANG=zh_CN.GB2312

#/bin/sh ./packageApplication.sh {branch} {version} {build} {email} {note} {upload_key} {upload_token} {upload_url} {debug} {teamid} {codesign} {uuid} {provising} {apitoken}'
CUSTOM_BRANCH_NAME=${1}
CUSTOM_VERSION_NUMBER=${2}
CUSTOM_BUILD_NUMBER=${3}
CUSTOM_EMAIL_ADDRESS=${4}
CUSTOM_MESSAGE=${5}
CUSTOM_PACKAGE_ID=${6}
CUSTOM_UOLOAD_TOKEN=${7}
CUSTOM_UPLOAD_URL=${8}
CUSTOM_CONFIG=${9}

shift 9

CUSTOM_TEAM_ID=${1}

CUSTOM_CODESIGN="${2}"

CUSTOM_UUID="${3}"

CUSTOM_PROVISING="${4}"

CUSTOM_API_TOKEN="${5}"

CUSTOM_BUNDLE="${6}"

CUSTOM_PARAMETERS=${7}

CUSTOM_FILE_NAME="${8}"

API_INTERFACE="http://127.0.0.1:8811/v2/packageDidFinish"

dirpath=`dirname ~/git_temp`
basePath="$dirpath/git_temp"

WORKSPACE_NAME="GlobalApp"
xcodeproj_name=''
gitPath=''

BUNDLE_ID='com.eco.global.app'
BUNDLE_ID_INTER='com.DR95test.InternationalEcovacsApp'
BUNDLE_ID_ENTER='com.vivien.demo'

echo '准备开始打包....'
echo $CUSTOM_TEAM_ID
eco $CUSTOM_PROVISING
echo $CUSTOM_CONFIG
echo $CUSTOM_CODESIGN
echo $CUSTOM_PARAMETERS
echo $CUSTOM_UUID
echo $CUSTOM_PACKAGE_ID
EXPORT_PLIST_PWD=''
if [ ${CUSTOM_BUNDLE} == ${BUNDLE_ID_ENTER} ]
then
EXPORT_PLIST_PWD="$server_path/enterprise_export.plist"
else
EXPORT_PLIST_PWD="$server_path/adhoc_export.plist"
fi

if [ ${CUSTOM_UUID} == "d43d394a-aac5-42ff-8754-b14f87971169" ]
then
EXPORT_PLIST_PWD="$server_path/dev_export.plist"
fi

if [ ${CUSTOM_UUID} == "92a18ca4-1214-4862-a94e-854c4b92c49b" ]
then
EXPORT_PLIST_PWD="$server_path/appstore_export.plist"
fi

echo $EXPORT_PLIST_PWD



gitPath="ssh://git@10.10.10.25:443/GlobalApp-iOS"
WORKSPACE_NAME="GlobalApp"
xcodeproj_name="GlobalApp"

timestamp="${CUSTOM_BRANCH_NAME}-${CUSTOM_VERSION_NUMBER}-`date +%s`"
cd ~/git_temp

mkdir ${timestamp}
cd ${timestamp}
mkdir "ipas"
git clone -b ${CUSTOM_BRANCH_NAME} ${gitPath}

CONFIG_FILE_NAME="GLBConfig.json"

IPA_DIR="${basePath}""/${timestamp}/ipas"

BUILD_CONFIG=${CUSTOM_CONFIG} #编译的方式,有Release,Debug

isWorkSpace=true

WORKSPACE_PWD=`find "${basePath}/${timestamp}" -name "${WORKSPACE_NAME}.xcworkspace"| awk -F [\w] '{print $(NF-2)}'`

WORKSPACE_DIR=`dirname ${WORKSPACE_PWD}`

BUILD_TEMP_DIR="$IPA_DIR/build"


GLOBAL_CONFIG_PWD=`find "${basePath}/${timestamp}" -name "${CONFIG_FILE_NAME}"| awk -F [\w] '{print $(NF)}'`

XCODEPROJ_PWD=`find "${basePath}/${timestamp}" -name "${xcodeproj_name}.xcodeproj"| awk -F [\w] '{print $(NF)}'`
XCODEPROJ_DIR=`dirname ${XCODEPROJ_PWD}`

INFO_PLIST_PWD=(`find "${XCODEPROJ_DIR}" -name "Info.plist" | awk -F [\w] '{print $(NF)}'`)
CUSTOM_VERSION_NUMBER=`/usr/libexec/PlistBuddy -c 'Print:CFBundleShortVersionString' ${INFO_PLIST_PWD}`
SETTINGS_PLIST_PWD=(`find "${WORKSPACE_DIR}" -name "settings.plist" | awk -F [\w] '{print $(NF)}'`)
xcodeproj_path=${XCODEPROJ_PWD}/project.pbxproj

setXcodeprojToManual(){
    LC_ALL=C
    XCPROJ_FILES=`(find "${WORKSPACE_DIR}" -name "*.xcodeproj" | awk -F [\w] '{print $(NF)}')`
    for i in ${XCPROJ_FILES}
        do
        echo "${i}/project.pbxproj"
        sed -i "" "s/ProvisioningStyle = .*;/ProvisioningStyle = Manual;/g" "${i}/project.pbxproj"
        sed -i "" "s/CODE_SIGN_STYLE = .*;/CODE_SIGN_STYLE = Manual;/g" "${i}/project.pbxproj"
        sed -i "" "s/DEVELOPMENT_TEAM = .*;$/DEVELOPMENT_TEAM = \"\" ;/g" "${i}/project.pbxproj"
        sed -i "" "s/DevelopmentTeam = .*;$/DevelopmentTeam = \"\";/g" "${i}/project.pbxproj"
        sed -i "" "s/PROVISIONING_PROFILE_SPECIFIER = .*;/PROVISIONING_PROFILE_SPECIFIER = \"\";/g" "${i}/project.pbxproj"
    done
}

alterSDKVersionNumber(){
    SDK_PLIST_DIR=`(find "${WORKSPACE_DIR}" -name "EcoRobotSDK.bundle" | awk -F [\w] '{print $(NF)}')`
    T=`date +%Y%m%d`
    for i in ${SDK_PLIST_DIR}
    do
        `/usr/libexec/PlistBuddy -c "Set :VersionDate ${T}" "${i}/Info.plist"`
    done
}


alterProjConfigFile(){
   if [ "$CUSTOM_PARAMETERS" = "" ];then
        echo '没有配置自定义参数，直接用代码里的打包！'
    else
        echo '准备替换自定义参数， 使用自定义配置参数打包'
        `echo ${CUSTOM_PARAMETERS}>${GLOBAL_CONFIG_PWD}`
    fi
}

alterMainProjVersionNumber(){
    #修改版本号，build号
    `/usr/libexec/PlistBuddy -c "Set :CFBundleShortVersionString ${CUSTOM_VERSION_NUMBER}" ${INFO_PLIST_PWD}`
    `/usr/libexec/PlistBuddy -c "Set :CFBundleVersion ${CUSTOM_BUILD_NUMBER}" ${INFO_PLIST_PWD}`
}

alterSignInfo(){
    LC_ALL=C
    #修改签名方式为手动
    sed -i "" "s/ProvisioningStyle = .*;/ProvisioningStyle = Manual;/g" $xcodeproj_path
    #修改bundle id
    sed -i "" "s/PRODUCT_BUNDLE_IDENTIFIER = ${BUNDLE_ID};/PRODUCT_BUNDLE_IDENTIFIER = ${CUSTOM_BUNDLE};/g" $xcodeproj_path
    sed -i "" "s/PRODUCT_BUNDLE_IDENTIFIER = ${BUNDLE_ID_ENTER};/PRODUCT_BUNDLE_IDENTIFIER = ${CUSTOM_BUNDLE};/g" $xcodeproj_path
    sed -i "" "s/PRODUCT_BUNDLE_IDENTIFIER = ${BUNDLE_ID_INTER};/PRODUCT_BUNDLE_IDENTIFIER = ${CUSTOM_BUNDLE};/g" $xcodeproj_path
    #修改TEAM ID
    sed -i "" "s/DEVELOPMENT_TEAM = \".*\";$/DEVELOPMENT_TEAM = $CUSTOM_TEAM_ID;/g" $xcodeproj_path
    sed -i "" "s/DevelopmentTeam = \".*\";$/DevelopmentTeam = $CUSTOM_TEAM_ID;/g" $xcodeproj_path
    sed -i "" "s/DEVELOPMENT_TEAM = .*;$/DEVELOPMENT_TEAM = $CUSTOM_TEAM_ID;/g" $xcodeproj_path
    sed -i "" "s/DevelopmentTeam = .*;$/DevelopmentTeam = $CUSTOM_TEAM_ID;/g" $xcodeproj_path
    #修改签名文件
    sed -i "" "s/PROVISIONING_PROFILE = \".*\";/PROVISIONING_PROFILE = \"${CUSTOM_UUID}\";/g" $xcodeproj_path
    sed -i "" "s/PROVISIONING_PROFILE = .*;/PROVISIONING_PROFILE = \"${CUSTOM_UUID}\";/g" $xcodeproj_path
    sed -i "" "s/PROVISIONING_PROFILE_SPECIFIER = \".*\";/PROVISIONING_PROFILE_SPECIFIER = \"${CUSTOM_PROVISING}\";/g" $xcodeproj_path
    sed -i "" "s/PROVISIONING_PROFILE_SPECIFIER = .*;/PROVISIONING_PROFILE_SPECIFIER = \"${CUSTOM_PROVISING}\";/g" $xcodeproj_path
    sed -i "" "s/CODE_SIGN_IDENTITY = \".*\";/CODE_SIGN_IDENTITY = \"${CUSTOM_CODESIGN}\";/g" $xcodeproj_path
    sed -i "" "s/\"CODE_SIGN_IDENTITY.*;$/\"CODE_SIGN_IDENTITY[sdk=iphoneos*]\" = \"${CUSTOM_CODESIGN}\";/g" $xcodeproj_path
}

buildProj(){
    if ${isWorkSpace} ; then  #判断编译方式
        echo  "开始编译workspace...."
        echo "xcodebuild archive -workspace ${WORKSPACE_PWD} -scheme ${xcodeproj_name} -configuration ${BUILD_CONFIG} -archivePath "${BUILD_TEMP_DIR}/$xcodeproj_name.xcarchive" CODE_SIGN_IDENTITY='"${CUSTOM_CODESIGN}"' PROVISIONING_PROFILE="${CUSTOM_UUID}"
-quiet"
        xcodebuild archive -workspace ${WORKSPACE_PWD} -scheme ${xcodeproj_name} -configuration ${BUILD_CONFIG} -archivePath "${BUILD_TEMP_DIR}/$xcodeproj_name.xcarchive" CODE_SIGN_IDENTITY="${CUSTOM_CODESIGN}" PROVISIONING_PROFILE="${CUSTOM_UUID}" -quiet
    else
        echo  "开始编译target...."
        xcodebuild archive -project ${XCODEPROJ_PWD} -scheme ${xcodeproj_name} -configuration ${BUILD_CONFIG} -archivePath "${BUILD_TEMP_DIR}/$xcodeproj_name.xcarchive" CODE_SIGN_IDENTITY="${CUSTOM_CODESIGN}" PROVISIONING_PROFILE="${CUSTOM_UUID}"
    fi
    #判断编译结果
    if test $? -eq 0
    then
        echo "~~~~~~~~~~~~~~~~~~~编译成功~~~~~~~~~~~~~~~~~~~"

    else
    echo "~~~~~~~~~~~~~~~~~~~编译失败~~~~~~~~~~~~~~~~~~~"
        curl -d "email=$CUSTOM_EMAIL_ADDRESS&success=0&msg='编译失败，检查分支${CUSTOM_BRANCH_NAME} 是否可以编译通过'&packageid=$CUSTOM_PACKAGE_ID" $API_INTERFACE
        rm -rf ~/git_temp/$timestamp
        exit 1
    fi
}

exportIpa(){
    archive_path="${BUILD_TEMP_DIR}/${xcodeproj_name}.xcarchive"
    IPA_PATH="${IPA_DIR}/"${CUSTOM_BRANCH_NAME}-${CUSTOM_VERSION_NUMBER}-`date +%Y-%m-%d`"-${xcodeproj_name}"
    xcodebuild -exportArchive  -archivePath ${archive_path} -exportPath  ${IPA_PATH} -exportOptionsPlist ${EXPORT_PLIST_PWD} CODE_SIGN_IDENTITY="${CUSTOM_CODESIGN}" PROVISIONING_PROFILE="${CUSTOM_UUID}"

    echo "xcodebuild -exportArchive  -archivePath ${archive_path} -exportPath  ${IPA_PATH} -exportOptionsPlist ${EXPORT_PLIST_PWD} CODE_SIGN_IDENTITY="${CUSTOM_CODESIGN}" PROVISIONING_PROFILE="${CUSTOM_UUID}""

    if test $? -eq 0
    then
        echo "~~~~~~~~~~~~~~~~~~~打包成功~~~~~~~~~~~~~~~~~~~"
    else
        echo "~~~~~~~~~~~~~~~~~~~打包失败~~~~~~~~~~~~~~~~~~~"
        curl -d "email=$CUSTOM_EMAIL_ADDRESS&note=$CUSTOM_MESSAGE&packageid=$CUSTOM_PACKAGE_ID&version=$CUSTOM_VERSION_NUMBER&iot_sdk_version=$IOT_SDK_VERSION&build_number=$BUILD_TIME&success=0&msg=编译成功，打包失败！" $API_INTERFACE
        exit 1
    fi
    filename="$CUSTOM_PACKAGE_ID.ipa"
    zip_dsym_filename="$CUSTOM_PACKAGE_ID.zip"
    if [ ${CUSTOM_CONFIG} == "Release" ]
    then
     zip -q -r $zip_dsym_filename "$archive_path/dSYMs/EcovacsHome.app.dSYM"
     cp "$zip_dsym_filename" "$server_path/static/ipas/${zip_dsym_filename}"
    fi

    if [ "$CUSTOM_FILE_NAME" == "" ];then
        cp "${IPA_PATH}/${xcodeproj_name}.ipa" "$server_path/static/ipas/${filename}"
    else
        cp "${IPA_PATH}/${xcodeproj_name}.ipa" "$server_path/static/ipas/${CUSTOM_FILE_NAME}"
    fi
    DOWNLOAD_URL="http://10.88.42.27:8822/static/ipas/${filename}"
    DOWNLOAD_dsym_URL="http://10.88.42.27:8822/static/ipas/${zip_dsym_filename}"
 }

setXcodeprojToManual
alterProjConfigFile
alterSignInfo
buildProj
BUILD_TIME=`/usr/libexec/PlistBuddy -c 'Print:appBuildTime' ${SETTINGS_PLIST_PWD}`
IOT_SDK_VERSION=`/usr/libexec/PlistBuddy -c 'Print:iotSDKVersion' ${SETTINGS_PLIST_PWD}`
exportIpa
curl -d "email=$CUSTOM_EMAIL_ADDRESS&note=$CUSTOM_MESSAGE&packageid=$CUSTOM_PACKAGE_ID&version=$CUSTOM_VERSION_NUMBER&iot_sdk_version=$IOT_SDK_VERSION&build_number=$BUILD_TIME&success=1" $API_INTERFACE
rm -rf ~/git_temp/$timestamp