# coding: utf-8

from tornado.web import RequestHandler
from subprocess import Popen, PIPE
from tornado.web import asynchronous
from tornado.gen import coroutine, Return, maybe_future
from tornado.httpclient import AsyncHTTPClient, HTTPRequest
from mail import sendmail
from version_handler import *
import os
import json
from business.packageBusiness import *
import base64

API_TOKEN = "b4ee24264e600b6962eb2dffe307cf92"

client = AsyncHTTPClient()

BUNDLE_ID = 'com.eco.global.app'
BUNDLE_ID_ENTER = 'com.vivien.demo'

TEAM_IDS = ['8LQQ7JTB74', 'C7G78L37X3']

PACKAGE_CONFIG_DEV = {'bundle_id': BUNDLE_ID, 'team_id': TEAM_IDS[-1], 'code_sign': "'iPhone Developer'",
                      'profile': 'global_dev_notification', 'profile_uuid': '3fecee0c-068b-4b21-8f7a-8c16bff20a91'}

PACKAGE_CONFIG_RELEASE = {'bundle_id': BUNDLE_ID, 'team_id': TEAM_IDS[-1], 'code_sign': "'iPhone Distribution'",
                          'profile': 'global_dev', 'profile_uuid': '4a5b9d7c-55b5-4550-910b-aecce3d9b26a'}

PACKAGE_CONFIG_APPSTORE = {'bundle_id': BUNDLE_ID, 'team_id': TEAM_IDS[-1], 'code_sign': "'iPhone Distribution'",
                           'profile': 'dis_appstore', 'profile_uuid': '92a18ca4-1214-4862-a94e-854c4b92c49b'}

PACKAGE_CONFIG_DEV_ENTERPRISE = {'bundle_id': BUNDLE_ID_ENTER, 'team_id': TEAM_IDS[0],
                                 'code_sign': "'iPhone Distribution'", 'profile': "'vivien_distribution'",
                                 'profile_uuid': '39a716da-3d76-488f-9374-fe2d23d66e69'}

PACKAGE_CONFIG_RELEASE_ENTERPRISE = {'bundle_id': BUNDLE_ID_ENTER, 'team_id': TEAM_IDS[0],
                                     'code_sign': "'iPhone Distribution'", 'profile': "'vivien_distribution'",
                                     'profile_uuid': '39a716da-3d76-488f-9374-fe2d23d66e69'}

PACKAGE_DEBUG = 1
PACKAGE_RELEASE = 2

PACKAGE_TYPE_DEV = 1
PACKAGE_TYPE_ADHOC = 2
PACKAGE_TYPE_ENTERPRISE = 3
PACKAGE_TYPE_APPSTORE = 4

basepath = os.path.split(os.path.realpath(__file__))[0]


def is_json(s):
    try:
        json.loads(s)
    except ValueError:
        return False
    return True


class InstallHandler(RequestHandler):
    @asynchronous
    @coroutine
    def get(self):
        self.render('Index/install.html')


class MainHandler(RequestHandler):
    @asynchronous
    @coroutine
    def get(self):
        ios_build_number = getIOSBuildNumber()
        android_build_number = getAndroidBuildNumber()
        ios_package_count = packageBusinessClass.getPackageCount(packagetype=PACKAGE_TYPE_IOS)
        android_package_count = packageBusinessClass.getPackageCount(packagetype=PACKAGE_TYPE_ANDROID)
        self.render('Index/index.html', ios_build_number=ios_build_number, android_build_number=android_build_number,
                    ios_package_count=ios_package_count, android_package_count=android_package_count)


class IOSPackage(RequestHandler):
    @asynchronous
    @coroutine
    def get(self):
        with open(basepath + '/ios_branches.txt') as f:
            output = ''.join(f.readlines())
            branches = [i.split('/')[-1] for i in output.split(' ') if i.find('refs/') != -1]
            f.close()
            self.render('Index/ios_index.html', branches=branches)


class InternationalMainHandler(RequestHandler):
    @asynchronous
    @coroutine
    def get(self):
        with open(basepath + '/android_branches.txt') as f:
            output = ''.join(f.readlines())
            branches = [i.split('/')[-1] for i in output.split(' ') if i.find('refs/') != -1]
            self.render('Index/android_index.html', branches=branches)


class PackageApplicationHandler(RequestHandler):
    @asynchronous
    @coroutine
    def post(self):
        branch_name = self.get_argument('branch', '')  # 分支名称
        version_number = self.get_argument('version', '0')  # 版本号
        build_number = self.get_argument('build', '0')  # build 号
        email_address = self.get_argument('email', '')  # 邮箱
        note = self.get_argument('note', '').encode('utf-8')
        debug = int(self.get_argument('debug', 1))  # debug or release
        sign = int(self.get_argument('sign', 1))  # 打包方式
        build = 'Release' if debug == PACKAGE_RELEASE else 'Debug'
        custom_file_name = self.get_argument('filename', '')
        packageid = ''  # 每次打包都有一个唯一id
        try:
            big_data_env = self.get_argument('big_data_env')
            it_api_env = self.get_argument('it_api_env')
            package_channel = self.get_argument('package_channel')
            intime_log_onoff = self.get_argument('intime_log_onoff', False)
            local_log_onoff = self.get_argument('local_log_onoff', False)
            console_log_onoff = self.get_argument('console_log_onoff', False)
            custom_parameters = '''"big_data_env": "{big_data_env}",
                    "it_api_env": "{it_api_env}",
                    "package_channel": "{package_channel}",
                    "intime_log_onoff": {intime_log_onoff},
                    "local_log_onoff": {local_log_onoff},
                    "console_log_onoff": {console_log_onoff}
            '''.format(big_data_env=big_data_env, it_api_env=it_api_env, package_channel=package_channel,
                       intime_log_onoff=intime_log_onoff, local_log_onoff=local_log_onoff,
                       console_log_onoff=console_log_onoff)

            custom_parameters = '{' + custom_parameters + '}'

            all_parameters = json.loads(custom_parameters)
            all_parameters['branch'] = branch_name
            all_parameters['email_address'] = email_address
            all_parameters['note'] = note
            all_parameters['debug'] = build
            all_parameters['sign'] = sign

            packageid = packageBusinessClass.newPackage(packagetype=PACKAGE_TYPE_IOS, config=json.dumps(all_parameters))
        except Exception, e:
            custom_parameters = ""

        if sign == PACKAGE_TYPE_ENTERPRISE:
            bundle_id = BUNDLE_ID_ENTER
        else:
            bundle_id = BUNDLE_ID

        teamid = TEAM_IDS[1] if bundle_id == BUNDLE_ID else TEAM_IDS[0]
        code_sign = ''
        profile_uuid = ''
        profile = ''
        if bundle_id == BUNDLE_ID:
            profile = PACKAGE_CONFIG_DEV['profile'] if sign == PACKAGE_TYPE_DEV else PACKAGE_CONFIG_RELEASE['profile']
            code_sign = PACKAGE_CONFIG_DEV['code_sign'] if sign == PACKAGE_TYPE_DEV else PACKAGE_CONFIG_RELEASE[
                'code_sign']
            profile_uuid = PACKAGE_CONFIG_DEV['profile_uuid'] if sign == PACKAGE_TYPE_DEV else \
                PACKAGE_CONFIG_RELEASE['profile_uuid']

            if sign == PACKAGE_TYPE_APPSTORE:
                profile = PACKAGE_CONFIG_APPSTORE['profile']
                code_sign = PACKAGE_CONFIG_APPSTORE['code_sign']
                profile_uuid = PACKAGE_CONFIG_APPSTORE['profile_uuid']

        elif bundle_id == BUNDLE_ID_ENTER:
            profile = PACKAGE_CONFIG_DEV_ENTERPRISE['profile'] if sign == PACKAGE_TYPE_DEV else \
                PACKAGE_CONFIG_RELEASE_ENTERPRISE['profile']
            code_sign = PACKAGE_CONFIG_DEV_ENTERPRISE['code_sign'] if sign == PACKAGE_TYPE_DEV else \
                PACKAGE_CONFIG_RELEASE_ENTERPRISE['code_sign']
            profile_uuid = PACKAGE_CONFIG_DEV_ENTERPRISE['profile_uuid'] if sign == PACKAGE_TYPE_DEV else \
                PACKAGE_CONFIG_RELEASE_ENTERPRISE['profile_uuid']
        if len(branch_name) * len(version_number) * len(build_number) * len(email_address):
            cmd = "/bin/sh {sh_path} {branch} {version} {build} '{email}' '{note}' '{packageid}' '{upload_token}' '{upload_url}' {debug} {teamid} {codesign} {uuid} {provising} {apitoken} {bundle} '{custom_parameters}'  {filename}".format(
                sh_path=basepath + '/packageApplication.sh', branch=branch_name, version=version_number,
                build=build_number, email=email_address, note=note, packageid=packageid, upload_token='', upload_url='',
                debug=build, teamid=teamid, codesign=code_sign, uuid=profile_uuid, provising=profile,
                apitoken=API_TOKEN, bundle=bundle_id, custom_parameters=custom_parameters, filename=custom_file_name)
            Popen(cmd, shell=True)

            self.write({'code': 200, 'msg': u'主人，马上为您打包，安装包链接稍后发送到您的邮箱：'})
        else:
            self.write({'code': 500, 'msg': u'参数错误'})
        self.finish()


class AndroidPackageApplicationHandler(RequestHandler):
    @asynchronous
    @coroutine
    def post(self):
        branch_name = self.get_argument('branch', '')  # 分支名称
        email_address = self.get_argument('email', '')  # 邮箱
        note = self.get_argument('note', '').encode('utf-8')  # 备注
        channel = self.get_argument('channel', '')  # 渠道包
        debuggable = self.get_argument('debuggable', True)
        it_api_env = self.get_argument('it_api_env')
        iot_env = self.get_argument('iot_env')
        intime_log_onoff = self.get_argument('intime_log_onoff', False)
        console_log_onoff = self.get_argument('console_log_onoff', False)
        jpush = self.get_argument('jpush')
        app_signature = self.get_argument('app_signature')
        proguard = self.get_argument('proguard')
        firebase = self.get_argument('firebase')
        xiaoneng = self.get_argument('xiaoneng')
        big_data_env = self.get_argument('big_data_env')

        custom_parameters = '''"debuggable": {debuggable}, "it_api_env": "{it_api_env}", "iot_env": "{iot_env}", "intime_log_onoff":
         {intime_log_onoff}, "console_log_onoff": {console_log_onoff}, "jpush":"{jpush}", "app_signature":"{app_signature}",
          "proguard":{proguard}, "firebase": "{firebase}", "xiaoneng": "{xiaoneng}", "big_data_env": "{big_data_env}"'''.format(
            debuggable=debuggable, it_api_env=it_api_env, iot_env=iot_env, intime_log_onoff=intime_log_onoff,
            console_log_onoff=console_log_onoff, jpush=jpush, app_signature=app_signature, proguard=proguard,
            firebase=firebase, xiaoneng=xiaoneng, big_data_env=big_data_env)
        custom_parameters = '{' + custom_parameters + '}'
        if channel == 'all':
            channel = ''

        custom_file_name = self.get_argument('filename', '')
        all_parameters = json.loads(custom_parameters)
        all_parameters['branch'] = branch_name
        all_parameters['email_address'] = email_address
        all_parameters['note'] = note
        all_parameters['channel'] = channel
        print all_parameters
        packageid = packageBusinessClass.newPackage(packagetype=PACKAGE_TYPE_ANDROID, config=json.dumps(all_parameters))

        if len(branch_name) * len(email_address) * len(note):
            cmd = "/bin/sh {sh_path} {branch} {debug} '{note}' '{email}' '{channel}' '{filename}' '{packageid}' '{custom_parameters}'".format(
                sh_path=basepath + '/packageAndroidApplication.sh', branch=branch_name, debug='autopackaging',
                note=note, email=email_address, channel=channel, filename=custom_file_name, packageid=packageid,
                custom_parameters=custom_parameters)
            Popen(cmd, shell=True)
            print cmd
            self.write({'code': 200, 'msg': u'主人，马上为您打包，安装包链接稍后发送到您的邮箱：'})
        else:
            self.write({'code': 500, 'msg': u'参数错误'})
        self.finish()

    @coroutine
    def getFirToken(self):
        req = HTTPRequest(method="POST", url="http://api.fir.im/apps", body=json.dumps(
            {"api_token": API_TOKEN, "type": "ios", "bundle_id": "com.DR95test.InternationalEcovacsApp"}),
                          headers={"Content-Type": "application/json"})
        data = yield client.fetch(request=req)
        raise Return(json.loads(data.body))


class PackageDidFinish(RequestHandler):
    @asynchronous
    @coroutine
    def post(self, *args, **kwargs):
        title = self.get_argument('title', '')
        email = self.get_argument('email')
        email = email.split(',')
        note = self.get_argument('note', '')
        msg = self.get_argument('msg', '')
        sendmail.exchange_send_email(mailist=email, subject=u'自动打包结果反馈' + title,
                                     msg=note + '\n\n' + msg + u'\n\n 本邮件由系统自动发出！请勿回复！')
        self.write({'msg': 'ok'})
        self.finish()


class PackageDidFinish1(RequestHandler):
    @asynchronous
    @coroutine
    def post(self, *args, **kwargs):
        packageid = self.get_argument('packageid')
        version = self.get_argument('version', '')
        iot_sdk_version = self.get_argument('iot_sdk_version', '')
        success = self.get_argument('success')
        build_number = self.get_argument('build_number', '')
        email = self.get_argument('email')
        email = email.split(',')
        note = self.get_argument('note', '')
        msg = self.get_argument('msg', '')
        type = int(self.get_argument('package_type', PACKAGE_TYPE_IOS))
        packageBusinessClass.updatePackage(packageid=packageid, version=version, iot_sdk_version=iot_sdk_version,
                                           success=success, build_number=build_number, email_list=email, note=note,
                                           type=type)

        title = u'ECOVACS HOME {version} {build_number} {iot_sdk_version}'.format(version=version,
                                                                                  build_number=build_number,
                                                                                  iot_sdk_version=iot_sdk_version)

        if type == PACKAGE_TYPE_IOS:

            title = u'【iOS自动打包结果】' + title

            if int(success):

                package = packageBusinessClass.getPackageById(packageid=packageid)
                package_config = json.loads(package.packageConfig)
                sign = package_config['sign']
                if sign == PACKAGE_TYPE_DEV:
                    sign = u"内测包（可测推送）"
                if sign == PACKAGE_TYPE_ADHOC:
                    sign = u"AdHoc包"
                if sign == PACKAGE_TYPE_ENTERPRISE:
                    sign = u"企业包"
                if sign == PACKAGE_TYPE_APPSTORE:
                    sign = u"AppStore包"

                package_info = u'【包参数】包签名：{sign}\n大数据环境：{big_data_env}\nIT环境：{it_api_env}\nIOT渠道：{package_channel}\n实时日志：{intime_log_onoff}\n本地日志：{local_log_onoff}\n控制台输出：{console_log_onoff}'.format(
                    big_data_env=u'正式环境' if package_config['big_data_env'] == u'release' else u'测试环境',
                    it_api_env=u'正式环境' if package_config['it_api_env'] == u'release' else u'测试环境',
                    package_channel=package_config['package_channel'],
                    intime_log_onoff=u'打开' if bool(package_config['intime_log_onoff']) else u'关闭',
                    local_log_onoff=u'打开' if bool(package_config['local_log_onoff']) else u'关闭',
                    console_log_onoff=u'打开' if bool(package_config['console_log_onoff']) else u'关闭', sign=sign)

                package_download = u'【包下载地址】：{package_down_url}'.format(package_down_url=package.packagePath)

                package_note = u'【提测信息】：{note}'.format(note=note)

                sendmail.exchange_send_email(mailist=email, subject=title,
                                             msg=package_info + '\n\n' + package_download + u'\n\n' + package_note + msg + u'\n\n 本邮件由系统自动发出！请勿回复！')
            else:
                sendmail.exchange_send_email(mailist=email, subject=title, msg=msg + u'\n\n 本邮件由系统自动发出！请勿回复！')

        else:
            title = u'【Android自动打包结果】' + title
            if int(success):

                package = packageBusinessClass.getPackageById(packageid=packageid)
                package_config = json.loads(package.packageConfig)
                channel = package_config['channel']
                if channel == '':
                    channel = u"全渠道"

                package_info = u'【包参数】包渠道：{channel}\n包是否可调试: {debuggable}\n大数据环境：{big_data_env}\nIT环境：{it_api_env}\nIOT渠道：{package_channel}\n实时日志：{intime_log_onoff}\n' \
                               u'控制台输出：{console_log_onoff}\n极光推送：{jpush}\n app签名：{app_signature}\n是否混淆:{proguard}\n谷歌推送:{firebase}\n客服小能: {xiaoneng}'.format(
                    big_data_env=u'正式环境' if package_config['big_data_env'] == u'release' else u'测试环境',
                    debuggable=u'可调式' if bool(package_config['intime_log_onoff']) else u'不可调试',
                    it_api_env=u'正式环境' if package_config['it_api_env'] == u'release' else u'测试环境',
                    package_channel=package_config['channel'],
                    intime_log_onoff=u'打开' if bool(package_config['intime_log_onoff']) else u'关闭',
                    console_log_onoff=u'打开' if bool(package_config['console_log_onoff']) else u'关闭', channel=channel,
                    jpush=u'正式环境' if package_config['jpush'] == u'release' else u'测试环境',
                    app_signature=u'正式环境' if package_config['app_signature'] == u'release' else u'测试环境',
                    proguard=u'混淆' if bool(package_config['proguard']) else u'不混淆',
                    firebase=u'正式环境' if package_config['firebase'] == u'release' else u'测试环境',
                    xiaoneng=u'正式环境' if package_config['firebase'] == u'release' else u'测试环境')

                package_download = u'【包下载地址】：{package_down_url}'.format(package_down_url=package.packagePath)

                package_note = u'【提测信息】：{note}'.format(note=note)

                sendmail.exchange_send_email(mailist=email, subject=title,
                                             msg=package_info + '\n\n' + package_download + u'\n\n' + package_note + msg + u'\n\n 本邮件由系统自动发出！请勿回复！')
            else:
                sendmail.exchange_send_email(mailist=email, subject=title, msg=msg + u'\n\n 本邮件由系统自动发出！请勿回复！')

        self.write({'msg': 'send email ok'})
        self.finish()

    @coroutine
    def getAllAppsFromFir(self):
        req = HTTPRequest(method="GET", url='http://api.fir.im/apps?api_token={key}'.format(key=API_TOKEN))
        client.fetch(req)
        data = yield client.fetch(req)
        raise Return(data.body)


class demo(RequestHandler):
    @asynchronous
    @coroutine
    def get(self):
        self.render('Index/demo.html')


class finger(RequestHandler):
    @asynchronous
    @coroutine
    def get(self):
        self.render('Index/finger.html')


class DownLoadHandler(RequestHandler):
    @asynchronous
    @coroutine
    def get(self):
        self.render('Index/items_download.html')


if __name__ == '__main__':
    print os.path.split(os.path.realpath(__file__))[0]
