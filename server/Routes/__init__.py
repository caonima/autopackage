#!/usr/bin/env python
# coding:utf-8

import tornado.web


class Route(object):
    urls = []

    def __call__(self, url, name=None):
        def _(cls):
            self.urls.append(tornado.web.URLSpec(url, cls, name=name))
            return cls

        return _


class Modules(object):
    modules = {}

    def __call__(self, name):
        def _(cls):
            self.modules[name] = cls
            return cls

        return _


module = Modules()

route = Route()
