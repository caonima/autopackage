from Models.packageModel import *
from Classes.ReturnValue import *
import bson
import json
import time


class packageBusinessClass():
    @classmethod
    def newPackage(cls, packagetype, config):
        package = packageModel()
        package.packageTime = int(time.time())
        package.packageType = packagetype
        package.packageConfig = config
        package.packagePath = ''
        package.packageState = PACKAGE_PACKING
        package.packageSymbolPath = ''
        res = package.save()
        return str(res.id)

    @classmethod
    def updatePackage(cls, packageid, email_list, version, iot_sdk_version, success, build_number, note, type):
        if type == PACKAGE_TYPE_IOS:
            success = int(success)
            package_path = 'http://10.88.42.27:8822/static/ipas/{name}.ipa'.format(name=packageid) if success else ''
            symbol_path = 'http://10.88.42.27:8822/static/ipas/{name}.zip'.format(name=packageid) if success else ''
            packageModel.objects(id=bson.ObjectId(packageid)).update(set__packageVersion=version,
                                                                     set__packageIOTVersion=iot_sdk_version,
                                                                     set__packageBuildVersion=build_number,
                                                                     packageEmailList=email_list,
                                                                     set__packageState=PACKAGE_SUCCESS if int(
                                                                         success) else PACKAGE_FAILED,
                                                                     set__packagePath=package_path,
                                                                     set__packageSymbolPath=symbol_path,
                                                                     set__packageNote=note)
        else:
            success = int(success)
            package_path = 'http://10.88.42.27:8822/static/apks/{name}.zip'.format(name=packageid) if success else ''
            packageModel.objects(id=bson.ObjectId(packageid)).update(set__packageVersion=version,
                                                                     set__packageIOTVersion=iot_sdk_version,
                                                                     set__packageBuildVersion=build_number,
                                                                     packageEmailList=email_list,
                                                                     set__packageState=PACKAGE_SUCCESS if int(
                                                                         success) else PACKAGE_FAILED,
                                                                     set__packagePath=package_path,
                                                                     set__packageSymbolPath='',
                                                                     set__packageNote=note)

    @classmethod
    def getPackageById(cls, packageid):
        return packageModel.objects(id=bson.ObjectId(packageid)).first()

    @classmethod
    def getPackageCount(cls, packagetype):
        return packageModel.objects(packageType=packagetype).count()


    @classmethod
    def updatePackageUploaded(cls, packageid):
        packageModel.objects(id=bson.ObjectId(packageid)).update(set__packageState=PACKAGE_UPLOADED)

    @classmethod
    def updatePackageUploading(cls, packageid):
        packageModel.objects(id=bson.ObjectId(packageid)).update(set__packageState=PACKAGE_UPLOADING)

    @classmethod
    def getPackages(cls, packagetype):
        array = []
        for i in packageModel.objects(packageType=packagetype).order_by('-id').limit(30):
            i.packageConfig = json.loads(i.packageConfig)
            i.packageTimeString = time.strftime("%Y-%m-%d %H:%M", time.localtime(i.packageTime))
            array.append(i)
        return array
