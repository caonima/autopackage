from Models.UserModel import *
from Classes.ReturnValue import *
from uuid import uuid1
import hashlib


class cmsAdminBusinessClass():
    def __init__(self):
        pass

    @classmethod
    def loginAction(cls, username, pwd):
        res = userModel.objects(username=username, userpwd=pwd)
        accessToken = cls.updateAccessToken(username)
        if res.count():
            return returnValue(code=CODE_OK, msg=MSG_OK,
                               result={'userid': str(res[0].id), 'accesstoken': accessToken, 'username': username})
        else:
            return returnValue(code=CODE_ERROR, msg=MSG_ERROR, result={})

    @classmethod
    def updateAccessToken(cls, tel):
        uuidStr = None
        try:
            uuidStr = hashlib.md5(str(uuid1())).hexdigest()
            userModel.objects(PhoneNumber=tel).update(set__AccessToken=uuidStr)
        except Exception, e:
            print e.message
        finally:
            return uuidStr

    @classmethod
    def checkAccessTokenWithUserId(cls, userid, accesstoken):
        user = userModel.objects(id=bson.ObjectId(userid), AccessToken=accesstoken).first()
        return True if user else False
