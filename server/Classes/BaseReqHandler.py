# coding:utf-8
from tornado.web import RequestHandler
from server.business.userBusiness import *


class BaseRequestHandler(RequestHandler):
    def __init__(self, application, request):
        RequestHandler.__init__(self, application, request)
        self.userAgentString = ''
        self.cookieString = ''
        self.parameters = {}
        self.userid = None
        self.accesstoken = None

    def prepare(self):
        for k, v in self.request.arguments.iteritems():
            self.parameters[k] = v[0]
        userId = self.get_cookie('userid')
        accessToken = self.get_cookie('accesstoken')
        if userId and accessToken:
            v = yield cmsAdminBusinessClass.checkAccessTokenWithUserId(userId, accessToken)
            if v:
                self.current_user = userId
                self.UserId = userId
                self.username = self.get_cookie('username')
                self.AccessToken = accessToken
            else:
                self.clear_all_cookies()
                self.current_user = None
        else:
            self.current_user = None
            self.clear_all_cookies()


def auth(method):
    def wrapper(self, *args, **kwargs):
        userid = self.userid
        accesstoken = self.accesstoken
        if userid and accesstoken and len(accesstoken) > 10:  # accesstoken 的长度肯定大于5
            if cmsAdminBusinessClass.checkAccessTokenWithUserId(userid=userid, accesstoken=accesstoken):
                return method(self, *args, **kwargs)
        self.write(returnValue(code=CODE_NO_RIGHT, msg=MSG_NO_ACCESS, result=None).returnValueDict)
        self.finish()

    return wrapper
