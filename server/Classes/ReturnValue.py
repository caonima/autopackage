# coding: utf-8

CODE_OK = 200

CODE_ERROR = 500

CODE_NO_RIGHT = 403

CODE_NO_VIP = 222

CODE_GUN = 9999

MSG_OK = u'ok'
MSG_ERROR = u'未知错误'
MSG_CAONIMA = u'傻逼出错啦, 不要问我为什么，自己找问题！'
MSG_SIGN_ERROR = u'傻逼签名错啦！'
MSG_NO_ACCESS = u'权限校验失败，请重新登录!'

BUSINESS_CODE_ERROR = 0
BUSINESS_CODE_OK = 1


class returnValue():
    def __init__(self, code, result, msg=''):
        self.code = code
        self.result = result
        self.msg = msg

    @property
    def returnValueDict(self):
        return {'code': self.code, 'result': self.result, 'msg': self.msg}

    @classmethod
    def noRightValueDict(cls):
        return {'code': CODE_NO_RIGHT, 'result': None, 'msg': '权限错误'}