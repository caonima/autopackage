#!/usr/bin/env python
# coding:utf-8


class MailSenderSingleton(object):
    __instance = None

    def __new__(cls, *args, **kwargs):
        if cls.__instance is None:
            cls.__instance = super(
                    MailSenderSingleton, cls).__new__(cls, *args, **kwargs)
        return cls.__instance

    def __init__(self, Credentials=None, Account=None):
        if Credentials and Account:
            self.Credentials = Credentials
            self.Account = Account

    def credentials(self):
        return self.Credentials

    def account(self):
        return self.Account
