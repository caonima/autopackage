#!/usr/bin/env python
# coding:utf-8
import tornado.web
from tornado.ioloop import IOLoop
from tornado.httpserver import HTTPServer
from os import path
from main import *
from version_handler import *
from exchangelib import Account, Credentials
from Classes.SingletonClass import *
from release_handler import *
from Routes import *

urls = [(r"/", MainHandler), (r"/ios_package/", IOSPackage), (r"/android_package/", InternationalMainHandler),
        (r'/preparePackage', PackageApplicationHandler), (r'/prepareAndroidPackage', AndroidPackageApplicationHandler),
        (r'/install', InstallHandler), (r'/ios_unique_version_number', Ios_UniqueIdNumberHandler),
        (r'/downloads/', DownLoadHandler), (r'/android_unique_version_number', Android_UniqueIdNumberHandler),
        (r'/android_release', Android_ReleaseHandler), (r'/ios_release', iOS_ReleaseHandler),(r'/upload_ipa', UploadIpa),(r'/upload_itc_response', UploadIpaResult),
        (r'/packageDidFinish', PackageDidFinish), (r'/v2/packageDidFinish', PackageDidFinish1), (r'/v2/finger', finger)]

credentials = Credentials('ecovacs\pei.zhou', 'eco@82840')
settings = {"Credentials": credentials,
            'Account': Account('pei.zhou@ecovacs.com', credentials=credentials, autodiscover=True)}

MailSenderSingleton(Credentials=settings['Credentials'], Account=settings['Account'])

application = tornado.web.Application(urls, debug=True,
                                      static_path=path.join(path.dirname(path.abspath(__file__)), 'static'),
                                      template_path=path.join(path.dirname(path.abspath(__file__)), 'template'),
                                      xsrf_cookies=False, login_url='/login/')

if __name__ == "__main__":
    server1 = HTTPServer(application, xheaders=True)
    server1.listen(8811)

    server2 = HTTPServer(application, xheaders=True)
    application.listen(8822)

    server2 = HTTPServer(application, xheaders=True)
    application.listen(8833)

    server2 = HTTPServer(application, xheaders=True)
    application.listen(8844)

    ioloop = IOLoop.instance()
    ioloop.set_blocking_log_threshold(1)
    ioloop.start()
