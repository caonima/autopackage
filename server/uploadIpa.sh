#!/bin/bash

set -e
set -u
#
IPA_PATH=${1}
ITC_USER=${2}
ITC_PASSWORD=${3}
PACKAGE_ID=${4}

API_INTERFACE="http://127.0.0.1:8811/upload_itc_response"


altool="$(dirname "$(xcode-select -p)")/Applications/Application Loader.app/Contents/Frameworks/ITunesSoftwareService.framework/Support/altool"

#echo "Validating app..."
#msg=`"$altool" --validate-app --type ios --file "$IPA_PATH" --username "$ITC_USER" --password "$ITC_PASSWORD"`
#echo $msg
echo "Uploading app to iTC..."
msg1=`"$altool" --upload-app --type ios --file "$IPA_PATH" --username "$ITC_USER" --password "$ITC_PASSWORD"`


curl -d "packageid=$PACKAGE_ID&msg=$""&msg1=$msg1" $API_INTERFACE
