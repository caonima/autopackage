# coding: utf-8

from tornado.web import RequestHandler
from tornado.web import asynchronous
from tornado.gen import coroutine, Return, maybe_future
import time
import os

PWD = os.path.dirname(os.path.abspath(__file__))

IOS_NUMBER_FILE = PWD + '/ios_number.txt'
ANDROID_NUMBER_FILE = PWD + '/android_number.txt'


class Ios_UniqueIdNumberHandler(RequestHandler):
    @asynchronous
    @coroutine
    def get(self):
        num = 0

        with open(IOS_NUMBER_FILE, 'r') as f:
            num = int(''.join(f.readlines()).strip())
            f.close()
        with open(IOS_NUMBER_FILE, 'w') as f:
            f.writelines(str(num + 1))
            f.close()

        self.write({'number': num, 't': int(time.time())})
        self.finish()


def getIOSBuildNumber():
    with open(IOS_NUMBER_FILE, 'r') as f:
        num = int(''.join(f.readlines()).strip()) - 12345
        f.close()
    return num


def getAndroidBuildNumber():
    with open(ANDROID_NUMBER_FILE, 'r') as f:
        num = int(''.join(f.readlines()).strip()) - 12345
        f.close()
    return num


class Android_UniqueIdNumberHandler(RequestHandler):
    @asynchronous
    @coroutine
    def get(self):
        num = 0
        with open(ANDROID_NUMBER_FILE, 'r') as f:
            num = int(''.join(f.readlines()).strip())
            f.close()
        with open(ANDROID_NUMBER_FILE, 'w') as f:
            f.writelines(str(num + 1))
            f.close()

        self.write({'number': str(num), 't': int(time.time())})
        self.finish()
