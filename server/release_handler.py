# coding: utf-8

from tornado.web import RequestHandler
from tornado.web import asynchronous
from tornado.gen import coroutine, Return, maybe_future
from business.packageBusiness import *
from subprocess import Popen, PIPE
from mail import sendmail

import os

PWD = os.path.dirname(os.path.abspath(__file__))


class Android_ReleaseHandler(RequestHandler):
    @asynchronous
    @coroutine
    def get(self):
        packages = packageBusinessClass.getPackages(packagetype=PACKAGE_TYPE_ANDROID)
        self.render('Index/android_release_page.html',packages=packages)


class iOS_ReleaseHandler(RequestHandler):
    @asynchronous
    @coroutine
    def get(self):
        packages = packageBusinessClass.getPackages(packagetype=PACKAGE_TYPE_IOS)
        self.render('Index/ios_release_page.html', packages=packages)


class UploadIpa(RequestHandler):
    @asynchronous
    @coroutine
    def post(self):
        packageid = self.get_argument('packageid')
        package = packageBusinessClass.getPackageById(packageid=packageid)
        packageBusinessClass.updatePackageUploading(packageid=packageid)
        if package:
            path = package.packagePath.split('ipas/')[1]
            path = PWD + '/static/ipas/' + path
            cmd = "/bin/sh {sh_path} {ipa_path} {itc_user} {itc_pwd} {packageid}".format(sh_path=PWD + '/uploadIpa.sh',
                                                                                         ipa_path=path,
                                                                                         itc_user='pei.zhou@ecovacs.com',
                                                                                         itc_pwd='lmcm-pjkf-gfpm-xhnm',
                                                                                         packageid=packageid)
            Popen(cmd, shell=True)

            self.write({'code': 200, 'msg': u'主人，即将上传IPA包到Appstore，稍后发送上传结果到您的邮箱'})
        else:
            self.write({'code': 500, 'msg': u'包好像不存在哦~'})
        self.finish()


class UploadIpaResult(RequestHandler):
    @asynchronous
    @coroutine
    def post(self):
        packageid = self.get_argument('packageid')
        packageBusinessClass.updatePackageUploaded(packageid=packageid)
        package = packageBusinessClass.getPackageById(packageid=packageid)
        msg = self.get_argument('msg')
        msg1 = self.get_argument('msg1')
        title = u'【上传AppStore结果】ECOVACS HOME {version} {build_number} {iot_sdk_version}'.format(
            version=package.packageVersion, build_number=package.packageBuildVersion,
            iot_sdk_version=package.packageIOTVersion)

        sendmail.exchange_send_email(mailist=package.packageEmailList, subject=title,
                                     msg=u'上传AppStore成功，稍后请登录上传AppStore后台操作TestFlight' + msg + msg1 + u'\n\n' + u'\n\n 本邮件由系统自动发出！请勿回复！')
        self.write(u'上传成功，发送邮件成功')
        self.finish()


if __name__ == '__main__':
    print PWD
