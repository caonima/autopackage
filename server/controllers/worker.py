import time
import datetime
import requests


def todayTimestamp():
    today = datetime.date.today()
    todayTimeStamp = time.mktime(datetime.datetime(today.year, today.month, today.day, 00, 00, 00).timetuple())
    return todayTimeStamp


def requestPackage4iOS():
    data = {'branch': 'DEV_D', 'email': 'pei.zhou@ecovacs.com', 'debug': 2, 'sign': 1, 'big_data_env': 'release',
            'it_api_env': 'release', 'package_channel': "c_appstore", 'intime_log_onoff': 'false',
            'local_log_onoff': 'false', 'console_log_onoff': 'false', 'note': 'ios Daily build',
            'filename': 'GlobalApp-%s.ipa' % str(todayTimestamp()).split('.')[0]}
    print requests.post('http://10.88.42.27:8811/preparePackage', data=data).content


def requestPackage4Android():
    data = {'branch': 'r_dev', 'email': 'pei.zhou@ecovacs.com', 'note': 'daily', 'channel': 'c_test',
            'debug': 'Release', 'filename': 'GlobalApp-%s' % str(todayTimestamp()).split('.')[0]}
    print requests.post('http://10.88.42.27:8811/prepareAndroidPackage', data=data).content


if __name__ == '__main__':
    print "start daily build========================"
    # print todayTimestamp()
    requestPackage4Android()
    requestPackage4iOS()
