#iOS项目组自动打包系统说明
##Requrements
+ python
+ tornado
+ shell
##开源地址
+ [Bitbucket主页](https://bitbucket.org/caonima/autopackage "")

##部署地址
ip：192.168.0.103 端口: 8811, 8822, 8833
访问地址为: http://192.168.0.103:8811
**wifi切到 ECOM2F-TEST6**

##使用帮助
**支持多人并发打包，互不影响，国内版打包过程大约8分钟，国际版本大约5分钟。无论成功与否，都会收到邮件通知。**

**分支:**

从远程仓库拉取所有分支名称

**打包方式:**

+ DEV 开发环境证书，推送只能走开发环境,只有注册过的机器可安装（BUNDLE_ID = 'com.ecovacs.ecosphere'
+ Adhoc 正式环境测试证书，推送走线上环境，只有注册过的机器可安装（BUNDLE_ID = 'com.DR95test.InternationalEcovacsApp'）
+ Enterprise 企业版证书，推送收不到，信任证书后，所有机器可安装（BUNDLE_ID = 'com.vivien.demo'）

**打包环境:**

+ Debug: 测试环境
+ Release: 产线环境

**版本号：**

当前版本号

**build号: **

build号

##特殊说明##
**国内版本SDK版本号目前每次打包会改成当前 “年月日”，如20170616**
**开发者后台所有带"pei.zhou"字样的证书请勿注销**

##未完成功能
对接AppStore,自动发布版本。

